const { Sequelize, Model, DataTypes } = require('sequelize');
const config = require('../../config').db.connection;
const sequelize = new Sequelize(config.database, config.user, config.password, {
    dialect: 'mariadb',
    host: config.host,
    port: config.port,
    sync: { force: true },
    pool: {
        min: 0,
        max: 5,
        idle: 10000
    },
});

sequelize.authenticate().then(done => {
    console.log('Connection has been established successfully.');
}).catch(err => {
    console.log('Unable to connect to the database:', err);
});

/**
 * Creating Sequelize Model Instances
 */

const User = sequelize.define('User', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING
    }
}, {});


/**
 * Synchronizing all models at once
 */
(async() => {
    await sequelize.sync({ force: true });
    console.log('All Models Synchronized');
})();