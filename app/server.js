const express = require('express');
const app = express();
const cors = require('cors')
const morgan = require('morgan');
const bodyParser = require('body-parser');
const { SERVER } = require('../config');
require('./database');

app.use(cors());
/**
 * Body Parser Middleware
 * parse application/json
 */
app.use(bodyParser.json())

/**
 * Body Parser Middleware
 * parse application/x-www-form-urlencoded
 */
app.use(bodyParser.urlencoded({ extended: false }))


/**
 * Description : Morgan is a popular HTTP request middleware logger for Node.js and basically used as a logger. 
 *               It can be used with node js' winston package to consolidate HTTP request data logs with other information.
 */
app.use(morgan('dev'))

/**
 * Set Public Folder
 */

app.get('', (req, res) => {
    res.status(200).json({
        msg: 'server is running'
    });
});


app.listen(SERVER._PORT, SERVER._PORT, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.error(`app running on https://${SERVER._HOST}:${SERVER._PORT}`);
        console.log('server is started');
    }
});