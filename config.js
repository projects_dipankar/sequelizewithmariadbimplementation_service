module.exports = {
    SERVER: {
        _PORT: 3009,
        _HOST: 'localhost',
        baseFolder: '/Users/dips_mac/Desktop/Projects//Users/dips_mac/Desktop/Projects/Personal_Projects/PracticeMariaDBOrm'
    },

    db: {
        connection: {
            database: 'sequelize_mariadb',
            host: 'localhost',
            port: 3306,
            user: 'user1_mac',
            password: 'password'
        },
    },
}